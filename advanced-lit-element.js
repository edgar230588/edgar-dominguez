import { LitElement, html } from 'lit-element'; // importación de Node Modules y lit element

class AdvancedLitElement extends LitElement {

    static get properties() {
        return {
            basicBind: { type: String },
            groceriesList: { type: Array },
            isAdmin: { type: Boolean }
        };
    }

    constructor() {   //Inicio de constructor
        super();
        this.basicBind = 'Hola Edd'; //Bienvenida
        this.groceriesList = ['Manzana', 'Banana', 'Avena']; // lista de compras
        this.isAdmin = true; //cambia dependiendo si es Admin o usuario
    }

    render() {  //devuelve los valores del objeto
            return html `
            <p>${this.basicBind}</p>
            <ul>
                ${this.groceriesList.map((food) => html`<li>${food}</li>`)} 
            </ul>
            ${this.isAdmin
            ? html`<p>Bienvenida, administradora</p>`
            : html`<p>Bienvenido, usuario</p>`}
        `;
    }
}

customElements.define('advanced-lit-element', AdvancedLitElement);